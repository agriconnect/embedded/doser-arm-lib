Library to parse response from ARM component in Automatic Doser.

## Build

```
meson _build
ninja -C _build
```

## Run test cases

```
ninja -C _build test
```
