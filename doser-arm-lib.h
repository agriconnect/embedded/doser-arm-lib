#ifndef __DOSERARM_H__
#define __DOSERARM_H__

#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "jsmn.h"

#define NUM_ENV_PARAMS 3
#define STR_EQUAL(str1, str2) (strcmp(str1, str2) == 0)
#define STR_NEQUAL(str1, str2) (strcmp(str1, str2) != 0)
#define STARTSWITH(str_original, head) (strncmp(str_original, head, strlen(head)) == 0)

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    ARM_UNDEFINED = 0,
    ARM_AA = 1,
    ARM_GA = 2,
    ARM_SA = 3,
    ARM_AE = 7,
} arm_cmd_t;

typedef struct {
    arm_cmd_t cmd;
    bool is_ok;
    /* Pointer to string representing "data" part of response */
    char *data;
    /* Pointer to string representing "error" part of response */
    char *err_data;
} arm_resp_t;

typedef struct {
    float ph;   // pH
    float ec;   // EC
    float st;   // Solution temperature
} env_cond_t;

void armrp_init(arm_resp_t *resp)
{
    resp->cmd = ARM_UNDEFINED;
    resp->is_ok = false;
    resp->data = NULL;
    resp->err_data = NULL;
}

/**
 * @param resp_string ARM's full response as a string.
 * @param resp Pointer to a struct to hold parsed response.
 * @return True if ARM response is valid
 **/
bool parse_arm_response(const char *resp_string, arm_resp_t *resp)
{
    const char delim[] = " ";
    char *token = NULL;
    size_t len = strlen(resp_string);
    char running[len + 1];
    strcpy(running, resp_string);
    token = strtok(running, delim);
    if (STR_NEQUAL(token, "OK") && STR_NEQUAL(token, "ERR")) {
        return false;
    }
    if (STR_EQUAL(token, "OK")) {
        resp->is_ok = true;
    }
    token = strtok(NULL, delim);
    if (STR_EQUAL(token, "AA")) {
        resp->cmd = ARM_AA;
    } else if (STR_EQUAL(token, "GA")) {
        resp->cmd = ARM_GA;
    } else if (STR_EQUAL(token, "SA")) {
        resp->cmd = ARM_SA;
    } else if (STR_EQUAL(token, "AE")) {
        resp->cmd = ARM_AE;
    }
    else {
        return false;
    }
    if (resp->is_ok) {
        token = strtok(NULL, "");
        /* token now point to the "data" part (JSON string) on "running" buffer.
         * We move resp->data to point to the corresponding part, but on resp_string buffer.
         * Note: We will get the same content, if we let "resp->data = token". But because "token"
         * and "running" buffer will be dellocated when exitting this function,
         * we should point resp->data on "resp_string" to be safe.
         */
        resp->data = (char *)resp_string + (token - running);
    }
    return true;
}

/**
 * @param data_string JSON string contains the data.
 * @param cond Pointer to a struct to hold parsed response.
 * @return Number of values which were successfulle parsed. -1 if error.
 **/
int parse_environment_data_string(const char *data_string, env_cond_t *cond)
{
    jsmn_parser jsParser;
    int r = 0;
    uint8_t count = 0;
    jsmn_init(&jsParser);
    // Try seeing how many token there are
    r = jsmn_parse(&jsParser, data_string, strlen(data_string), NULL, 1);
    if (r < 0) {
        return -1;
    }
    uint8_t num_tokens = r;
    jsmntok_t tokens[num_tokens];
    r = 0;
    jsmn_init(&jsParser);
    r = jsmn_parse(&jsParser, data_string, strlen(data_string), tokens, num_tokens);
    if (r < 1 || tokens[0].type != JSMN_OBJECT) {
        // Top token is not an object
        return -1;
    }
    for (uint8_t i = 0; i < (r - 1) / 2; i++) {
        uint8_t j = i * 2 + 1;
        jsmntok_t ftoken = tokens[j];
        jsmntok_t v = tokens[j + 1];
        const char *field = data_string + ftoken.start;
        const char *value = data_string + v.start;
        if (STARTSWITH(field, "PH")) {
            cond->ph = strtof(value, NULL);
            count++;
        } else if (STARTSWITH(field, "EC")) {
            cond->ec = strtof(value, NULL);
            count++;
        } else if (STARTSWITH(field, "ST")) {
            cond->st = strtof(value, NULL);
            count++;
        }
    }
    return count;
}

#ifdef __cplusplus
}
#endif

#endif /* __DOSERARM_H__ */
