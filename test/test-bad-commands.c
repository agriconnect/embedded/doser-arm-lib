#include <stdbool.h>
#include <unity.h>
#include "doser-arm-lib.h"

void setUp(void) {
    // Nothing to do
}

void tearDown(void) {
    // Nothing to do
}

void test_unknown_status(void)
{
    char resp_string[] = "XX AA {\"1\": 0, \"2\": 1}";
    arm_resp_t resp;
    bool is_valid;
    armrp_init(&resp);
    is_valid = parse_arm_response(resp_string, &resp);
    TEST_ASSERT_EQUAL(is_valid, false);
}

void test_response_error(void)
{
    char resp_string[] = "ERR AA ";
    arm_resp_t resp;
    armrp_init(&resp);
    parse_arm_response(resp_string, &resp);
    TEST_ASSERT_EQUAL(resp.cmd, ARM_AA);
    TEST_ASSERT_FALSE(resp.is_ok);
    TEST_ASSERT_NULL(resp.data);
}

int main(void)
{
    UNITY_BEGIN();
    RUN_TEST(test_unknown_status);
    RUN_TEST(test_response_error);
    return UNITY_END();
}
