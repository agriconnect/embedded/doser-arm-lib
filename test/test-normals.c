#include <stdbool.h>
#include <unity.h>
#include "doser-arm-lib.h"

void setUp(void) {
    // Nothing to do
}

void tearDown(void) {
    // Nothing to do
}

void test_aa(void)
{
    char resp_string[] = "OK AA {\"1\": 0, \"2\": 1}";
    arm_resp_t resp;
    bool is_valid;
    armrp_init(&resp);
    is_valid = parse_arm_response(resp_string, &resp);
    TEST_ASSERT_EQUAL(is_valid, true);
    TEST_ASSERT_EQUAL(resp.cmd, ARM_AA);
    TEST_ASSERT_TRUE(resp.is_ok);
    TEST_ASSERT_EQUAL_STRING(resp.data, "{\"1\": 0, \"2\": 1}");
}

void test_ga(void)
{
    char resp_string[] = "OK GA {\"2\": 1}";
    arm_resp_t resp;
    armrp_init(&resp);
    parse_arm_response(resp_string, &resp);
    TEST_ASSERT_EQUAL(resp.cmd, ARM_GA);
    TEST_ASSERT_EQUAL_STRING(resp.data, "{\"2\": 1}");
}

void test_sa(void)
{
    char resp_string[] = "OK SA {\"2\": 1}";
    arm_resp_t resp;
    armrp_init(&resp);
    parse_arm_response(resp_string, &resp);
    TEST_ASSERT_EQUAL(resp.cmd, ARM_SA);
    TEST_ASSERT_EQUAL_STRING(resp.data, "{\"2\": 1}");
}

void test_ae(void)
{
    char resp_string[] = "OK AE {\"PH\": 6, \"EC\": 500, \"ST\": 26}";
    arm_resp_t resp;
    armrp_init(&resp);
    parse_arm_response(resp_string, &resp);
    TEST_ASSERT_EQUAL(resp.cmd, ARM_AE);
    TEST_ASSERT_EQUAL_STRING(resp.data, "{\"PH\": 6, \"EC\": 500, \"ST\": 26}");
}

int main(void)
{
    UNITY_BEGIN();
    RUN_TEST(test_aa);
    RUN_TEST(test_ga);
    RUN_TEST(test_sa);
    RUN_TEST(test_ae);
    return UNITY_END();
}
