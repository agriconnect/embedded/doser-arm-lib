#include <stdbool.h>
#include <unity.h>
#include "doser-arm-lib.h"

void setUp(void) {
    // Nothing to do
}

void tearDown(void) {
    // Nothing to do
}

void test_parse_environment_full(void)
{
    char data_string[] = "{\"PH\": 6, \"EC\": 500, \"ST\": 26}";
    env_cond_t cond = {0, 0, 0};
    int r = 0;
    r = parse_environment_data_string(data_string, &cond);
    TEST_ASSERT_EQUAL_INT(r, 3);
    TEST_ASSERT_EQUAL_FLOAT(cond.ph, 6.0);
    TEST_ASSERT_EQUAL_FLOAT(cond.ec, 500.0);
    TEST_ASSERT_EQUAL_FLOAT(cond.st, 26.0);
}

void test_parse_environment_part(void)
{
    char data_string[] = "{\"PH\": 6, \"EC\": 500}";
    env_cond_t cond = {0, 0, 0};
    int r = 0;
    r = parse_environment_data_string(data_string, &cond);
    TEST_ASSERT_EQUAL_INT(r, 2);
    TEST_ASSERT_EQUAL_FLOAT(cond.ph, 6.0);
    TEST_ASSERT_EQUAL_FLOAT(cond.ec, 500.0);
    TEST_ASSERT_EQUAL_FLOAT(cond.st, 0);
}

int main(void)
{
    UNITY_BEGIN();
    RUN_TEST(test_parse_environment_full);
    RUN_TEST(test_parse_environment_part);
    return UNITY_END();
}
